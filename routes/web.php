<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', [MainController::class, 'index']);

Route::group(['prefix' => 'cms'], function () {
    Auth::routes([
        'register' => false, // Registration Routes...
        'reset' => false, // Password Reset Routes...
        'verify' => false, // Email Verification Routes...
    ]);
});

Route::group(['middleware' => ['auth']], function(){
    Route::post('post/upload', [PostController::class, 'upload'])->name('post.upload');
    Route::resource('posts', PostController::class);

    Route::patch('users/update-profile', [UserController::class, 'update_profile'])->name('users.update-profile');
    Route::get('users/edit-profile', [UserController::class, 'edit_profile'])->name('users.edit-profile');
    Route::resource('users', UserController::class);
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('articles', [PostController::class, 'list'])->name('articles.list');
Route::get('articles/{slug}', [PostController::class, 'detail'])->name('articles.detail');
