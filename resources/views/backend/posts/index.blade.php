@extends('layouts.backend.dashboard.master')

@section('title', 'List Posts')

@section('content')
<div class="container">
    @include('layouts.backend.dashboard.include.message')
    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="{{ route('posts.create') }}" class="btn btn-success mb-4">Create</a>
        </div>
    </div>
    <div class="row">
        @forelse ($posts as $post)    
            <div class="col-md-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-9">
                                <h5 class="card-title">{{ $post->title }}</h5>
                            </div>
                            <div class="col-md-3">
                                <span class="badge badge-{{ $post->status == 'inactive' ? 'danger' : 'success' }}">
                                    {{ $post->status == 'inactive' ? 'Inactive' : 'Active' }}
                                </span>
                            </div>
                        </div>
                        <p class="card-text">{!! substr($post->description,0,100) !!}...</p>
                        
                        @can('update-post', $post)
                            <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary">Edit</a>
                        @endcan

                        @can('delete-post', $post)    
                            <button id="delete" data-title="{{ $post->title }}"
                                href="{{ route('posts.destroy', $post->id) }}" class="btn btn-danger btn-sm d-inline">
                                <i class="zmdi zmdi-delete zmdi-hc-fw" style="color: #fff; font-size: 1.5em;"></i>
                                Delete
                            </button>
                        @endcan

                        <form method="post" id="deleteForm">
                            @csrf
                            @method("DELETE")
                            <input type="submit" value="" style="display:none;">
                        </form>
                    </div>
                </div>
            </div>
        @empty
            <h4 class="text-center">No Posts</h4>
        @endforelse
    </div>
</div>
@endsection

@push('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $('button#delete').on('click', function(){
            var href = $(this).attr('href');
            var title = $(this).data('title');

            swal({
                title: "Apakah Anda yakin ingin menghapus "+ title,
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    document.getElementById('deleteForm').action = href;
                    document.getElementById('deleteForm').submit();
                    swal("Video berhasil dihapus!", {
                        icon: "success",
                    });
                }
            });
        });
    </script>
@endpush